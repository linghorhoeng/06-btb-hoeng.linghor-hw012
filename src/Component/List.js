import React from 'react';
import { Link, Switch, Route } from 'react-router-dom'
function List() {
  const AniList =({match})=><h3>{match.params.topic}</h3>;
  return (
    <div>
    <h1>Animation Categoery</h1>
    <ul>
    <li>
        <Link to="/Video/Animation/Action">Action</Link>
    </li>
    <li>
        <Link to="/Video/Animation/Romance">Romance</Link>
    </li>
    <li>
        <Link to="/Video/Animation/Comedy">Comedy</Link>
    </li>
</ul>
    <Route exact path="/Video/Animation/:topic" component={AniList}></Route>
    
  </div>

  );
}

export default List;

