
import { Route, Switch,BrowserRouter as Router } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react';
import Menu from "./Component/Menu";
import Auth from "./Component/Auth";
import Account from "./Component/Account";
import Video from "./Component/Video";
import Home from "./Component/Home";
import Main from "./Component/Main";
import Seemore from "./Component/Seemore";
import Welcome from './Component/Welcome';
export default class App extends Component {

  render() {
    return (
      <div className="App">
      <Router>
      <Menu/>
      <Switch>
      <Route  path='/' exact component={Main}/>
      <Route  path='/Home' component={Home}/>
      <Route path="/Seemore/:id" component={Seemore} />
      <Route  path='/Account' component={Account}/>
      <Route  path='/Video' component={Video}/>
      <Route  path='/Auth' component={Auth}/>
      <Route path='/Welcome' component={Welcome} />
      )}
    />
      </Switch>
      </Router>
      </div>
    );
  }
}
