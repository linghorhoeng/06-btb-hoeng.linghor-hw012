import React from 'react';
import {Link } from "react-router-dom";
import queryString from "query-string";

function Account(props) {
  const parsed = queryString.parse(props.location.search);
  return (
    <div className="container" style={{padding:"20px"}}>
      <h1>Account</h1>
      <ul>
      <li>
      <Link to="/Account/?name=netflix">Netflix</Link>
    </li>
    <li>
      <Link to="/Account/?name=Zillow">Zillow Group</Link>
    </li>
    <li>  
      <Link to="/Account/?name=yahoo">Yahoo</Link>
    </li>
    <li>
      <Link to="/Account/?name=Modus">Modus Create</Link>
    </li>
      </ul>
    
        <h1>
          The <span style={{ color: "pink" }}>name</span> in the querystring is{" "}
          {`"${parsed.name}"`}
        </h1>
    </div>
  );
}


export default Account;
