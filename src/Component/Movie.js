import React, { Component } from 'react';
import { Link, Switch, Route } from 'react-router-dom'
export default class Movie extends Component {
  render() {
    const MovieList =({match})=><h3>{match.params.topic}</h3>;
    return (
      <div>
      <h1>Movie Categoery</h1>
      <ul>
      <li>
          <Link to="/Video/Movie/Adventure">Adventure</Link>
      </li>
      <li>
          <Link to="/Video/Movie/Comedy">Comedy</Link>
      </li>
      <li>
          <Link to="/Video/Movie/Crime">Crime</Link>
      </li>
      <li>
          <Link to="/Video/Movie/Documentary">Documentary</Link>
      </li>
  </ul>

  <Route exact path="/Video/Movie/:topic" component={MovieList}></Route>
      </div>
    );
  }
}
