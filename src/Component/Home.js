import React from 'react';
import {Card,Button } from "react-bootstrap";
import {Link} from 'react-router-dom';

function Home() {
    const elements = [1, 2, 3];
    let data = elements.map((id) => (
      <div className="col-4">

      <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src= "https://i.pinimg.com/originals/05/e8/55/05e8554a571d3a2b0c60c9659e3d4b6b.jpg"/>
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Text>
          Some quick example text to build on the card title and make up the bulk of
          the card's content.
        </Card.Text>
        <Link to={`/Seemore/${id}`}> <Button variant="primary">See more</Button></Link>
      </Card.Body>
    </Card>

    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src= "https://i.pinimg.com/originals/45/18/fb/4518fbc6d7ff90ae6fd7446d47aa4ce2.jpg"/>
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Text>
          Some quick example text to build on the card title and make up the bulk of
          the card's content.
        </Card.Text>
        <Link to={`/Seemore/${id}`}> <Button variant="primary">See more</Button></Link>
      </Card.Body>
    </Card>

    <Card style={{ width: '18rem' }}>
    <Card.Img variant="top" src= "https://wallpaperaccess.com/full/1761712.jpg"/>
    <Card.Body>
      <Card.Title>Card Title</Card.Title>
      <Card.Text>
        Some quick example text to build on the card title and make up the bulk of
        the card's content.
      </Card.Text>
      <Link to={`/Seemore/${id}`}> <Button variant="primary">See more</Button></Link>
    </Card.Body>
  </Card>

      </div>
    ));
    return (
      <div className="container" style={{ paddingLeft: "8%", paddingRight: "8%" }} >
        <div className="row">
        {data}
        </div>
      </div>
    );
  }
export default Home;
