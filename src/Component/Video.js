
import {Link,Route,Switch,Router} from 'react-router-dom';
import React, { Component } from 'react';
import List from './List';
import Movie from "./Movie";
const Type =({match})=> {
if(match.params.topic === "Animation"){
  return <Route path = '/Video/Animation' component={List}/>;
}else if(match.params.topic === "Movie"){
  return <Route path = '/Video/Movie' component={Movie}/>;
}
};
export default class Video extends Component {
  render() {
    return (
      <div className="container">
      <ul>
          <li>
              <Link to="/Video/Animation">Animation</Link>
          </li>
          <li>
              <Link to="/Video/Movie">Movie</Link>
          </li>
      </ul>
      <Route exact path="/Video"><h2>Please Choose A Topic:</h2></Route>
     <Route path="/Video/:topic" component={Type}/>
      </div>
    );
    }
}

